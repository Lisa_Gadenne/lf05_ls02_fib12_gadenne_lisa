import java.util.ArrayList;
import java.util.Random;
/**
 * 
 * @author Lisa Gadenne
 *
 */
public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladungsverzeichnis;
	
	public Raumschiff (int photonentorpedoAnzahl, int energieversorgungInProzent, 
						int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemeInProzent,
						int androidenAnzahl, String schiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
		this.broadcastKommunikator = new ArrayList<String>();
		this.ladungsverzeichnis = new ArrayList<Ladung>();
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		schiffsname = schiffsname;
	}

	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}

	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}
	/**
	 * {@summary Neue Ladung dem Ladungsverzeichnis mit Angabe von Bezeichnung & Menge hinzuf�gen}
	 * @param neueLadung
	 */
	public void addLadung (Ladung neueLadung) {
		ArrayList<Ladung> Ladung = this.ladungsverzeichnis;
		Ladung.add(neueLadung);
		this.setLadungsverzeichnis(Ladung);
	}
	//alles �ber diesem Kommentar ^ Aufgabe 4.1
	// Beginn der Methoden f�r Aufgabe 4.2

	/**
	 * {@summary Alle Zust�nde des jeweiligen Raumschiffes sollen mit deren Namen ausgegeben werden.}
	 * @return Zusatnd des Schiffes wird zur�ckgegeben
	 */
	public String zustandRaumschiff () {
		return("Schiffsname: "+ this.schiffsname +"\n"+
				           "H�lle in Prozent: "+this.huelleInProzent +"\n"+
				           "Schilde in Prozent: "+this.schildeInProzent+"\n"+
				           "Energieversorgung in Prozent: "+this.energieversorgungInProzent+"\n"+
				           "Lebenserhaltungssysteme in Prozent: "+this.lebenserhaltungssystemeInProzent+"\n"+
				           "Androidenanzahl: "+this.androidenAnzahl+"\n"+
				           "Photonentorpedoanzahl: "+this.photonentorpedoAnzahl+"\n"+
				           "Brodcastkommunikator: "+this.broadcastKommunikator+"\n"+
				           "Ladungsverzeichnis: "+this.ladungsverzeichnis+"\n");
	}

	/**
	 * {@summary Die Ladung eines Raumschiffes soll auf der Konsole ausgegeben werden.}
	 */
	public void ladungsverzeichnisAusgeben () {
		System.out.println("Ladungsverzeichnis: ");
		for (Ladung l: this.ladungsverzeichnis) {
			System.out.println(" Bezeichnung: " +l.getBezeichnung() + " \n Menge: " + l.getMenge());
		}
	}
	

	/**
	 * {@summary Wenn Menge einer Ladung 0, dann soll diese aus dem Ladungsverzeichnis gel�scht werden.}
	 */
	public void ladungsverzeichnisAufraeumen() {

        for(int i = 0 ;i < ladungsverzeichnis.size();i++) {
            if(ladungsverzeichnis.get(i).getMenge() == 0) {
                ladungsverzeichnis.remove(i);
                }
        }

        System.out.println("Ladung wurde bereinigt");
    }

	/**
	 * {@summary Die Nachrichten an Alle sollen dem Broadcast-Kommunikator hinzugef�gt werden.}
	 * @param message
	 */
	public void nachrichtAnAlle (String message) {
		broadcastKommunikator.add(message);
	}
	

	/**
	 * {@summary Schilde eines Raumschiffes um 50% geschw�cht, bei voller Zerst�rung dieser H�lle um 50% geschw�cht.
	 * Wenn H�lle danach zerst�rt dann Lebenserhaltungssysteme vollkommen zerst�rt + NAchticht an Alle �ber diese Zerst�rung}
	 * @param r
	 */
	private void trefferVermerken (Raumschiff r) {
        System.out.println(r.getSchiffsname() + " wurde getroffen!");
    //Methodenerweiterung
        if (r.getSchildeInProzent() < 1) {
            if (r.getHuelleInProzent() < 1) {
                nachrichtAnAlle("H�lle zerst�rt, Lebenserhaltungssysteme vernichtet");
                r.setLebenserhaltungssystemeInProzent(0);
            //huelle zertoert
            }else {
                r.setHuelleInProzent(r.getHuelleInProzent() - 50);
                r.setEnergieversorgungInProzent(r.getEnergieversorgungInProzent()-50);

            }
        //schilde noch vorhanden
        }else {
            r.setSchildeInProzent(r.getSchildeInProzent() - 50);
        }
    }
	

	/**
	 * {@summary Sind Torpedos vorhanden wird deren Anzahl um 1 reduziert & Nachricht an Alle "Photonentorpedo abgeschossen", 
	 * wenn nicht dann Nachricht an Alle "-=*Click*=-"}
	 * @param r
	 */
	public void photonentorpedoSchiessen (Raumschiff r) {
		if (photonentorpedoAnzahl <= 0) {
			nachrichtAnAlle("-=*Click*=-");
		}
		else {
			photonentorpedoAnzahl -= 1;
			nachrichtAnAlle("Photonentorpedo abgeschossen");
			trefferVermerken(r);
		}
	}
	

	/**
	 * {@summary Bei 50% Energie Versorgung soll eine Nachricht an alle gesendet werden. 
	 * Bei mehr als 50% wird energieversorgung um 50% reduziert und als Nachricht an Alle "Phaserkanone abgeschossen"}
	 * @param r
	 */
	public void phaserkanoneSchiessen (Raumschiff r) {
		if (energieversorgungInProzent < 50) {
			nachrichtAnAlle("-=*Click*=-");
		}
		else {
			energieversorgungInProzent -= 50;
			nachrichtAnAlle("Phaserkanone abgeschossen");
			trefferVermerken(r);
		}
	}
	

	/**
	 * {@summary Die Nachrichten aus dem Broadcast-Kommunikator sollen im Logbuch wiedergegeben werden.}
	 * @return der Broadcast-Kommunikator wird zur�ckgegeben
	 */
	public ArrayList<String> eintraegeLogbuchZurueckgeben (){
		return this.broadcastKommunikator;
	}
	

	/**
	 * {@summary In dieser Methode werden Photonentorpedos aus der Ladung nachgeladen, wenn vorhanden}
	 * @param anzahlTorpedos
	 */
	public void photonentorpedosLaden(int anzahlTorpedos) {
        int photoneni = 0;
        boolean vorhanden = false;

        for(int i = 0 ;i < ladungsverzeichnis.size();i++) {
            vorhanden = ladungsverzeichnis.get(i).getBezeichnung() == "Photonentorpedo";
                photoneni = i;
                if(vorhanden) {
                    break;
                }


        }
        if(!vorhanden) {
            System.out.println("Keine Photonentorpedos gefunden!");
            nachrichtAnAlle("-=*Click*=-");
        }else {
            System.out.println(anzahlTorpedos + "Photonentorpedo(s) eingesetzt");
            int menge = ladungsverzeichnis.get(photoneni).getMenge();
            if(anzahlTorpedos <= ladungsverzeichnis.get(photoneni).getMenge()) {
                photonentorpedoAnzahl += anzahlTorpedos;

                ladungsverzeichnis.get(photoneni).setMenge(menge - anzahlTorpedos);
            }else {
                System.out.println("nicht genug nur die menge ="+ menge +"wurde gelanden");

                photonentorpedoAnzahl += menge;

                ladungsverzeichnis.get(photoneni).setMenge(0);

            }

        }

    }
	

	/**
	 * 
	 * {@summary Die Reparatur von bestimmten Schiffsstrukturen soll durchgef�hrt werden.} 
	 * @param schutzschilde
	 * @param energieversorgung
	 * @param schiffshuelle
	 * @param anzahlDroiden
	 * 
	 */
	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden){
	    
	    int eingesetzt = anzahlDroiden;
	    if(anzahlDroiden > this.androidenAnzahl) {
	        eingesetzt = this.androidenAnzahl;
	    }
	    
	    int anzStrukturen = 0;            // anschlie�end maximal 3
	    if(schutzschilde) 
	        anzStrukturen += 1;
	    if(energieversorgung) 
	        anzStrukturen += 1;
	    if(schiffshuelle) 
	        anzStrukturen += 1;
	    
	    // Berechne drei Summanden f�r die Prozents�tze, 
	    // welche jeweils durch den Zufall beeinflusst sind.
	    int[] prozents�tze = new int[3];
	    for(int i=0; i<3; i++) {
	        int rand = (int) (Math.random()* 100);              // Zufallszahl zwischen 0 und 100
	        prozents�tze[i] =  rand* eingesetzt / anzStrukturen;
	    }
	    
	    
	    this.schildeInProzent             += schutzschilde?         prozents�tze[0]        : 0;
	    this.energieversorgungInProzent += energieversorgung?    prozents�tze[1]     : 0;        
	    this.huelleInProzent             += schiffshuelle?         prozents�tze[2]        : 0;    

	    
	    
	}
	
	/*public String toString() {
		return this.schiffsname +" | "+ this.photonentorpedoAnzahl +" | "+ this.androidenAnzahl +" | "+ this.energieversorgungInProzent +" | "+ this.huelleInProzent +" | "+ this.lebenserhaltungssystemeInProzent +" | "+ this.schildeInProzent;
	}*/
}

