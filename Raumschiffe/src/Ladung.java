
public class Ladung {

	private String bezeichnung;
	private int menge;
	
	public Ladung () {
		this.bezeichnung = "unbekannt";
		this.menge = 0; 
	}
	
	public Ladung (String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}
	public String getBezeichnung() {
		return this.bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	public int getMenge() {
		return this.menge;
	}
	public void setMenge (int menge) {
		this.menge = menge;
	}
	/**
	 * {@summary Die Bezeichnung einer ladung und die vorhandene Menge dieser soll zur�ckgegeben werden}
	 * @return Die Bezeichnung einer ladung und die vorhandene Menge dieser soll zur�ckgegeben werden
	 */
	@Override
	public String toString() {
	    return "Ladung: " + "|" + this.bezeichnung +":" +this.menge + "|";
	}
}
